import logging
import urlparse

# Non-stdlib imports
import pymongo

from bson import ObjectId
from tg import expose, redirect, validate
from formencode import validators
from pylons import tmpl_context as c, app_globals as g
from pylons import request
from tg.controllers import CUSTOM_CONTENT_TYPE
import ew.jinja2_ew as ew
import json
from urlparse import urlparse

# Pyforge-specific imports
from allura import model as M
from allura.app import Application, SitemapEntry, DefaultAdminController
#from allura.app import ConfigOption
from allura.controllers import BaseController
from allura.lib import helpers as h
from allura.lib.security import has_access, require_access
from allura.lib.search import search, SolrError
from allura.lib.widgets.search import SearchResults

# Local imports
from forgebookmarks import version
from forgebookmarks import widgets
from forgebookmarks import model as PBM

log = logging.getLogger(__name__)

class ForgeBookmarks(Application):
    '''This is the Link app for PyForge'''
    __version__ = version.__version__
    permissions = [ 'configure', 'read', 'write']
    config_options = Application.config_options + [
        #ConfigOption('url', str, None)
    ]
    searchable=True
    tool_label=version.__tool_label__
    default_mount_label=version.__default_mount_label__
    default_mount_point=version.__default_mount_point__
    #status='beta'
    ordinal=1
    
    search_results = SearchResults()
    
    # The ../../ are used to exit out of the Allura theme directory and then enter the tool's directory
    icons={
        24:'../../tool/bookmarks/images/bookmarks_24.png',
        32:'../../tool/bookmarks/images/bookmarks_32.png',
        48:'../../tool/bookmarks/images/bookmarks_48.png'
    }

    def __init__(self, project, config):
        Application.__init__(self, project, config)
        self.root = RootController()
        self.admin = AdminController(self)

    @property
    @h.exceptionless([], log)
    def sitemap(self):
        menu_id = self.config.options.mount_label.title()
        return [SitemapEntry(menu_id, '.')[self.sidebar_menu()] ]

    def sidebar_menu(self):
        links = []
        if has_access(self, 'write')():
            links.append(SitemapEntry('New BookMK',
                self.config.url() + 'create', ui_icon=g.icons['plus']))
        links.append(SitemapEntry('Recent BookMKs',
            self.config.url()))
        return links

    def admin_menu(self):
        return super(ForgeBookmarks, self).admin_menu()

    def install(self, project):
        'Set up any default permissions and roles here'
        super(ForgeBookmarks, self).install(project)
        # Setup permissions
        role_anon = M.ProjectRole.anonymous()._id
        role_member = M.ProjectRole.by_name('Member')._id
        role_admin = M.ProjectRole.by_name('Admin')._id
        self.config.acl = [
            M.ACE.allow(role_anon, 'read'),
            M.ACE.allow(role_member, 'write'),
            M.ACE.allow(role_admin, 'configure'),
            ]

    def uninstall(self, project):
        "Remove all the tofrom tg import expose, validateol's artifacts from the database"
        super(ForgeBookmarks, self).uninstall(project)

class W():
    bookmk_form = widgets.BookMKForm

class RootController(BaseController):
    def _check_security(self):
        require_access(c.app, 'read')

    @expose()
    def _lookup(self, bookmk_id, *remainder):
        return BookMKController(bookmk_id), remainder
	
	# Create new bookmarks screen
    @expose('jinja:forgebookmarks:templates/index.html')
    def create(self, bookmk=None, link=None, title=None, submitted=None, **kw):
        require_access(c.app, 'write')
        # Where a bookmark is stored within Mongo
        
        # Error checking
        errors=[]
        
        if submitted != None:
            if not title:
                errors.append('No title inputted')

            if not link:
                errors.append('No link inputted')
            else:
                bookmk_link = urlparse(link)
                if not (bookmk_link.scheme == 'http' or bookmk_link.scheme == 'https'):
                    errors.append('Not a valid link')
            if not bookmk:
                errors.append('No description inputted')
        
            # If there are no errors, submit this to the database
            if not errors:
                p = PBM.BookMK(text=bookmk, link=link, title=title)
                redirect(p.url())
        
        c.bookmk_form = W.bookmk_form()
        return dict(errors=errors, title=title, link=link, bookmk=bookmk)

	# Recent Bookmarks screen
    @expose('jinja:forgebookmarks:templates/bookmks.html')
    def index(self, **kw):
        # Where you actually query Mongo and get all the bookmarks
        bookmks = PBM.BookMK.query.find().sort('mod_date',
                pymongo.DESCENDING).limit(10).all()
        return dict(bookmks=bookmks)
    
    @expose('jinja:forgebookmarks:templates/search.html')
    @validate(dict(q=validators.UnicodeString(if_empty=None),
                   history=validators.StringBool(if_empty=False),
                   project=validators.StringBool(if_empty=False)))
    def search(self, q=None, history=None, project=None, limit=None, page=0, **kw):

        'local search'
        if project:
            redirect(c.project.url() + 'search?' + urlencode(dict(q=q, history=history)))
        search_error = None
        results = []
        count=0
        limit, page, start = g.handle_paging(limit, page, default=25)
        if not q:
            q = ''
        else:
            try:
                results = search(
                    q, short_timeout=True, ignore_errors=False,
                    rows=limit, start=start,
                    fq=[
                        'is_history_b:%s' % history,
                        'project_id_s:%s' % c.project._id,
                        'mount_point_s:%s'% c.app.config.options.mount_point,
                        '-deleted_b:true'])
            except SolrError as e:
                search_error = e
            if results: count=results.hits
        
        c.search_results = ForgeBookmarks.search_results
		
        return dict(q=q, history=history, results=results or [],
                    count=count, limit=limit, page=page, search_error=search_error)


class BookMKController(BaseController):
    def _check_security(self):
        require_access(c.app, 'read')

    def __init__(self, bookmk_id):
        if bookmk_id.endswith('.js'):
            bookmk_id, ext = bookmk_id.split('.')
        self.bookmk = PBM.BookMK.query.get(_id=ObjectId(bookmk_id))

    @expose('jinja:forgebookmarks:templates/bookmk/bookmk.js', content_type='text/javascript')
    @expose('jinja:forgebookmarks:templates/bookmk/bookmk.html')
    def index(self, **kw):
        if request.path.endswith('.js'):
            request.response_type = 'text/javascript'
            csslink = ew.CSSLink('tool/%s/%s' % (c.app.config.tool_name.lower(), 'css/pygments.css'))
            g.resource_manager.register(csslink)
            css = urlparse.urljoin('//%s' % request.host, csslink.url())
            return dict(bookmk=json.dumps(self.bookmk.html()), js=True, csslink=json.dumps(css))
        else:
            return dict(bookmk=self.bookmk.html(), js=False)

class AdminController(DefaultAdminController):
    pass
