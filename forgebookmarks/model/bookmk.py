from ming.orm import FieldProperty, Mapper

from allura import model as M

# This defines what is in each bookmark, including its properties.
class BookMK(M.Artifact):
    class __mongometa__:
        name='bookmk'
        #indexes = [ ]
    type_s='BookMK'

    title = FieldProperty(str, if_missing='')
    link = FieldProperty(str, if_missing='')
    text = FieldProperty(str, if_missing='')

	# Builds the HTML for the individual bookmark page
    def html(self, linenos=True):
        the_html = '<h1><a href="'+self.link+'">'+self.title+'</a></h1>'
        
        if self.text:
			the_html += '<p>Description: '+self.text+'</p>'
        
        return the_html

    def index(self):
        result = super(BookMK, self).index()
        
        result.update(
            title_s='Bookmark %s' % self.title,
            version_i=self.version,
            type_s='Bookmark',
            text=self.text)
        
        return result

    def index_id(self):
        return 'BookMK:%s' % self._id

    @property
    def lines(self):
        return len(self.text.split('\n'))

    def shorthand_id(self):
        return str(self._id) # pragma no cover

    def snippet(self, lines=5):
        return '\n'.join(self.text.split('\n')[:lines])

    def url(self):
        return (self.app_config.url() + str(self._id))

Mapper.compile_all()
