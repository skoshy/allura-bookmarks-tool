from alluratest.controller import TestController

class Test(TestController):
   
    def test_with_kwargs(self):
        r = self.app.get('/bookmarks/?x=y')
        assert 'Recent BookMKs' in r
