import ew as ew_core
from ew import jinja2_ew as ew

from allura.lib.widgets import form_fields as ffw
from allura.lib.widgets import forms as f

from forgebookmarks import version

#from formencode import validators as fev

class BookMKDesc(ffw.AutoResizeTextarea):
    def resources(self):
        for r in super(BookMKDesc, self).resources(): yield r
        yield ew.JSLink('js/jquery.textarea.js')

class BookMKLink(ew.TextField):
	pass

class BookMKTitle(ew.TextField):
	pass

'''
class BookMKLanguage(ew.SingleSelectField):

    def options(self):
        from pygments.lexers import get_all_lexers
        langs = sorted([(item[0], item[1][0])
                        for item in get_all_lexers()])
        options = [ew.Option(html_value='', label='&lt;auto detect&gt;')]
        options.extend([ew.Option(html_value=l[1], label=l[0])
                        for l in langs])
        return options
'''

class BookMKForm(f.ForgeForm):
    template='jinja:forgebookmarks:templates/widgets/bookmk_form.html'
    
    @property
    def fields(self):
        fields = ew_core.NameList(super(BookMKForm, self).fields)
        
        # Here's where you define what types of things to store in Mongo
        fields.append(BookMKDesc(name='bookmk',
            attrs={'style':'height:10em; width:745px; font-family:monospace'}))
        fields.append(BookMKLink(name='link', attrs={'style':'width: 400px;'}))
        fields.append(BookMKLink(name='title'))
        
        return fields

    def display_field_by_name(self, idx,  ignore_errors=False):
        field = self.fields[idx]
        ctx = self.context_for(field)
        display = field.display(**ctx)
        if ctx['errors'] and field.show_errors and not ignore_errors:
            display = "%s<div class='error'>%s</div>" % (display, ctx['errors'])
        return display
