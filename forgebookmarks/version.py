__version_info__ = (0, 0, 1)
__version__ = '.'.join(map(str, __version_info__))

__tool_label__ = 'Bookmarks'
__default_mount_label__='Bookmarks'
__default_mount_point__='bookmarks'

__tool_directory__ = 'forgebookmarks'
